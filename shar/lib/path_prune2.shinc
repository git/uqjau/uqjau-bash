#!/bin/false -meant2bSourced

# --------------------------------------------------------------------
# Synopsis: accepts exactly 1 arg - a colon delimited PATH, then
#   delete the directory entries in that list from a copy of system
#   PATH, echo the new PATH to STDOUT
# --------------------------------------------------------------------
# Usage: source $ourname in bash, then run for ex:
#     new_path=$(path_prune /usr/lib/lapack:/usr/bin:/bin)
#     echo $new_path
#     path_prune_unsource
# --------------------------------------------------------------------
# Rating: tone: tool used: daily stable: y TBDs: n interesting: y noteworthy: y comments: use in bash login sequence
# --------------------------------------------------------------------

# ====================================================================
# RCS INFORMATION (do not touch fields bounded by $)
# --------------------------------------------------------------------
#     $Date: 2010/10/24 14:28:39 $   (GMT)
# $Revision: 1.3 $
#   $Author: rodmant $ (aka last changed by)
#   $Locker: rodmant $
#   $Source: /usr/local/7Rq/package/cur/bash/shar/lib/RCS/path_prune2.shinc,v $
#      $Log: path_prune2.shinc,v $
#      Revision 1.3  2010/10/24 14:28:39  rodmant
#      *** empty log message ***
#
#      Revision 1.2  2010/01/22 01:54:09  rodmant
#      *** empty log message ***
#
#      Revision 1.1  2009/07/18 23:55:48  rodmant
#      Initial revision
#
# --------------------------------------------------------------------

# ====================================================================
# Copyright (c) 2010 Tom Rodman <Rodman.T.S@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# This could be turned into a script, but it's probably faster not
# to start a new shell

## IMP: arrays are global in bash

append2_PRUNED_ARRAY()
{
  # modifies array PRUNED_ARRAY
  # used by function 'mk_PRUNED_ARRAY'

  local appendthis="$1"
  if test ${#PRUNED_ARRAY[*]} -gt 0
  then
    PRUNED_ARRAY=( "${PRUNED_ARRAY[@]}" "$appendthis" )
  else
    PRUNED_ARRAY=(                      "$appendthis" )
  fi
}

mk_PRUNED_ARRAY()
{
  # creates "${PRUNED_ARRAY[@]}"  using:
  #   "$@" and "$dir2del"
  # requires function 'append2_PRUNED_ARRAY'

  local d
  PRUNED_ARRAY=()
  for d in "$@"
  do 
    case $d in
      "$dir2del")
        # will not be added
      ;;
      *)
        append2_PRUNED_ARRAY "$d"
      ;;
    esac
  done
}

_join()
{
  delim=$1
  shift
  # join args into 1 string with $delim

  local x d
  for d in "$@"
  do
    x=${x:+"${x}"$delim}"$d"
  done

  echo "$x"
}

set_path_as_array()
{
  local old_IFS="$IFS"   
  IFS=:
  set $PATH
  IFS="$old_IFS"
  path_as_array=("$@")
    # "$@" now is an array of dirs in $PATH

}

## main

path_prune()
{
  set_path_as_array
  PRUNED_ARRAY=( "${path_as_array[@]}" )

  local old_IFS="$IFS"
  IFS=:
  set $*
  IFS="$old_IFS"

  if test "$#" -gt 0
  then
    for d 
    do
      dir2del="$d"        mk_PRUNED_ARRAY "${PRUNED_ARRAY[@]}"
    done
  fi

  echo "$(_join : "${PRUNED_ARRAY[@]}")"

}

path_prune_unsource()
{
  unset dir2del path_as_array
  unset -f append2_PRUNED_ARRAY mk_PRUNED_ARRAY _join path_prune
  unset -f set_path_as_array path_prune_unsource
}

